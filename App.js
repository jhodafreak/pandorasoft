import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import AppNavigator from './routes/routes'; 

export default function App() {
  return (
    <NavigationContainer initialRouteName="Login">      
      <AppNavigator />
    </NavigationContainer>
  );
}

