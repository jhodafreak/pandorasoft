import React, { useState } from "react";
import {
  View,
  Text,
  Image,
  StyleSheet,
  TouchableOpacity,
  TextInput,
} from "react-native";
import logo from "../assets/logo.png";

const Login = ({ navigation }) => {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");

  const handleLogin = () => {
    navigation.navigate("Dashboard");
  };

  return (
    <View style={styles.container}>
      <View style={styles.ctnLogin}>
        <Image style={styles.logo} source={logo}></Image>
        <Text style={styles.title}>Bienvenido</Text>

        <View style={styles.ctnInputs}>
          <TextInput
            style={styles.input}
            placeholder="Ingrese su Usuario"
            value={username}
            onChangeText={(text) => setUsername(text)}
          />

          <TextInput
            style={styles.input}
            placeholder=" Ingrese su Contraseña"
            secureTextEntry={true}
            value={password}
            onChangeText={(text) => setPassword(text)}
          />

          <Text style={styles.txtPassRecorder}>Olvidaste tu contraseña?</Text>

          <TouchableOpacity
            title="Iniciar sesión"
            onPress={handleLogin}
            style={styles.button}
          >
            <Text style={styles.txtButton}>Iniciar Sesión</Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column",
    backgroundColor: "#fff",
  },

  ctnLogin: {
    flex: 1,
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center",
    top:40
  },

  ctnInputs: {
    flex: 1,
    flexDirection: "column",
    gap: 10,
  },

  button: {
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 30,
    color: "#fff",
    width: 300,
    height: 60,
    backgroundColor: "#FD9222",
  },

  logo: {
    width: 240,
    height: 150,
  },

  title: {
    color: "#FD9222",
    fontSize: 24,
    fontWeight: "bold",
    marginBottom: 20,
  },

  txtButton: {
    color: "#fff",
    fontWeight: "bold",
    textAlignVertical: "center",
  },

  txtPassRecorder:{
    textAlign:"center"
  },

  input: {
    width: 300,
    padding: 10,
    borderWidth: 1,
    borderColor: "#FD9222",
    borderRadius: 10,
    marginBottom: 10,
  },
});

export default Login;
